package com.boomi.connector.sftp.operations;import static org.mockito.Mockito.doThrow;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.boomi.connector.api.ObjectDefinitionRole;
import com.boomi.connector.api.OperationType;
import com.boomi.connector.sftp.SFTPClient;
import com.boomi.connector.sftp.SFTPConnector;
import com.boomi.connector.sftp.constants.SFTPConstants;
import com.boomi.connector.testutil.ConnectorTester;
import com.boomi.connector.testutil.SimpleBrowseContext;
import com.boomi.connector.testutil.SimpleOperationContext;
import com.boomi.connector.testutil.SimpleTrackedData;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;

@PowerMockIgnore("javax.management.*")
@RunWith(PowerMockRunner.class)
@PrepareForTest(SFTPClient.class)
public class SFTPGetOperationTest {

	

	@Test
    public void testSFTPGetOperation() throws Exception {
        JSch jsch = mock(JSch.class);
        Session session = mock(Session.class);
        ChannelSftp channel = mock(ChannelSftp.class);

 

        PowerMockito.whenNew(JSch.class).withNoArguments().thenReturn(jsch);
        when(jsch.getSession(any(String.class), any(String.class), any(Integer.class))).thenReturn(session);
        when(session.openChannel(any(String.class))).thenReturn(channel);
        
        Map<String, Object> opProperty = new HashMap<>();
        Map<String, Object> connProperty = new HashMap<>();
        opProperty.put(SFTPConstants.PROPERTY_INCLUDE_METADATA, Boolean.TRUE);
        opProperty.put(SFTPConstants.PROPERTY_DELETE_AFTER, Boolean.TRUE); 
        connProperty.put(SFTPConstants.AUTHORIZATION_TYPE, "Using public Key");
        connProperty.put(SFTPConstants.IS_MAX_EXCHANGE, Boolean.FALSE);
        connProperty.put(SFTPConstants.USE_KEY_CONTENT, false);
        new SimpleBrowseContext(null, null, OperationType.GET, connProperty,
                opProperty);

 

        new SimpleOperationContext(null, null, OperationType.GET, null, null,
                null, null);
        SFTPConnector connector = new SFTPConnector();

 

        ConnectorTester tester = new ConnectorTester(connector);
        Map<ObjectDefinitionRole, String> cookie = new HashMap<>();
        cookie.put(ObjectDefinitionRole.OUTPUT, "true");
        tester.setOperationContext(OperationType.GET, connProperty, opProperty, "File", cookie);
        // tester.setBrowseContext(context);
        tester.setBrowseContext(OperationType.GET, connProperty, opProperty);
        List<InputStream> streamList = new ArrayList<>();
        streamList.add(new ByteArrayInputStream("abc".getBytes("UTF-8")));

 

        Map<String, String> dynamicProperty = new HashMap<>();
        dynamicProperty.put(SFTPConstants.PROPERTY_FILENAME, "file1.txt");
        
        new SimpleTrackedData(1, new ByteArrayInputStream("abc".getBytes("UTF-8")),
                null, dynamicProperty); 
        
        tester.executeGetOperation("abc");
    }

	@Test
    public void testSFTPGetOperationDeleteDisabled() throws Exception { 
        JSch jsch = mock(JSch.class);
        Session session = mock(Session.class);
        ChannelSftp channel = mock(ChannelSftp.class);

 

        PowerMockito.whenNew(JSch.class).withNoArguments().thenReturn(jsch);
        when(jsch.getSession(any(String.class), any(String.class), any(Integer.class))).thenReturn(session);
        when(session.openChannel(any(String.class))).thenReturn(channel);
        
        Map<String, Object> opProperty = new HashMap<>();
        Map<String, Object> connProperty = new HashMap<>();
        opProperty.put(SFTPConstants.PROPERTY_INCLUDE_METADATA, Boolean.TRUE);
        opProperty.put(SFTPConstants.PROPERTY_DELETE_AFTER, Boolean.FALSE); 
        connProperty.put(SFTPConstants.AUTHORIZATION_TYPE, "Using public Key");
        connProperty.put(SFTPConstants.IS_MAX_EXCHANGE, Boolean.FALSE);
        connProperty.put(SFTPConstants.USE_KEY_CONTENT, false);
        new SimpleBrowseContext(null, null, OperationType.GET, connProperty,
                opProperty);

 

        new SimpleOperationContext(null, null, OperationType.GET, null, null,
                null, null);
        SFTPConnector connector = new SFTPConnector();

 

        ConnectorTester tester = new ConnectorTester(connector);
        Map<ObjectDefinitionRole, String> cookie = new HashMap<>();
        cookie.put(ObjectDefinitionRole.OUTPUT, "true");
        tester.setOperationContext(OperationType.GET, connProperty, opProperty, "File", cookie);
        // tester.setBrowseContext(context);
        tester.setBrowseContext(OperationType.GET, connProperty, opProperty);
        List<InputStream> streamList = new ArrayList<>();
        streamList.add(new ByteArrayInputStream("abc".getBytes("UTF-8")));

 

        Map<String, String> dynamicProperty = new HashMap<>();
        dynamicProperty.put(SFTPConstants.PROPERTY_FILENAME, "file1.txt");
        
        new SimpleTrackedData(1, new ByteArrayInputStream("abc".getBytes("UTF-8")),
                null, dynamicProperty); 
        
        tester.executeGetOperation("abc");
    }
	@Test
    public void testSFTPGetOperationFailAfterDelete() throws Exception { 
        JSch jsch = mock(JSch.class);
        Session session = mock(Session.class);
        ChannelSftp channel = mock(ChannelSftp.class);

 

        PowerMockito.whenNew(JSch.class).withNoArguments().thenReturn(jsch);
        when(jsch.getSession(any(String.class), any(String.class), any(Integer.class))).thenReturn(session);
        when(session.openChannel(any(String.class))).thenReturn(channel);
    //    when(channel.rm((any(String.class))).thenThrow(new SFTPSdkException("Error"));
        doThrow(new SftpException(2,"Error occurred")) 
        .when(channel).rm((any(String.class))); 
        Map<String, Object> opProperty = new HashMap<>();
        Map<String, Object> connProperty = new HashMap<>();
        opProperty.put(SFTPConstants.PROPERTY_INCLUDE_METADATA, Boolean.TRUE);
        opProperty.put(SFTPConstants.PROPERTY_DELETE_AFTER, Boolean.TRUE); 
        opProperty.put(SFTPConstants.PROPERTY_FAIL_DELETE_AFTER, Boolean.TRUE); 
        connProperty.put(SFTPConstants.AUTHORIZATION_TYPE, "Using public Key");
        connProperty.put(SFTPConstants.IS_MAX_EXCHANGE, Boolean.FALSE);
        connProperty.put(SFTPConstants.USE_KEY_CONTENT, false);
        new SimpleBrowseContext(null, null, OperationType.GET, connProperty,
                opProperty);

 

        new SimpleOperationContext(null, null, OperationType.GET, null, null,
                null, null);
        SFTPConnector connector = new SFTPConnector();

 

        ConnectorTester tester = new ConnectorTester(connector);
        Map<ObjectDefinitionRole, String> cookie = new HashMap<>();
        cookie.put(ObjectDefinitionRole.OUTPUT, "true");
        tester.setOperationContext(OperationType.GET, connProperty, opProperty, "File", cookie);
        // tester.setBrowseContext(context);
        tester.setBrowseContext(OperationType.GET, connProperty, opProperty);
        List<InputStream> streamList = new ArrayList<>();
        streamList.add(new ByteArrayInputStream("abc".getBytes("UTF-8")));

 

        Map<String, String> dynamicProperty = new HashMap<>();
        dynamicProperty.put(SFTPConstants.PROPERTY_FILENAME, "file1.txt");
        
        new SimpleTrackedData(1, new ByteArrayInputStream("abc".getBytes("UTF-8")),
                null, dynamicProperty); 
        
        tester.executeGetOperation("abc");
    }
	@Test
    public void testSFTPGetOperationDeleteFailed() throws Exception { 
        JSch jsch = mock(JSch.class);
        Session session = mock(Session.class);
        ChannelSftp channel = mock(ChannelSftp.class); 

 

        PowerMockito.whenNew(JSch.class).withNoArguments().thenReturn(jsch);
        when(jsch.getSession(any(String.class), any(String.class), any(Integer.class))).thenReturn(session);
        when(session.openChannel(any(String.class))).thenReturn(channel);
    //    when(channel.rm((any(String.class))).thenThrow(new SFTPSdkException("Error"));
        doThrow(new SftpException(3,"Error occurred")) 
        .when(channel).rm((any(String.class))); 
        Map<String, Object> opProperty = new HashMap<>();
        Map<String, Object> connProperty = new HashMap<>();
        opProperty.put(SFTPConstants.PROPERTY_INCLUDE_METADATA, Boolean.TRUE);
        opProperty.put(SFTPConstants.PROPERTY_DELETE_AFTER, Boolean.TRUE); 
        opProperty.put(SFTPConstants.PROPERTY_FAIL_DELETE_AFTER, Boolean.TRUE); 
        connProperty.put(SFTPConstants.AUTHORIZATION_TYPE, "Using public Key");
        connProperty.put(SFTPConstants.IS_MAX_EXCHANGE, Boolean.FALSE);
        connProperty.put(SFTPConstants.USE_KEY_CONTENT, false);
        new SimpleBrowseContext(null, null, OperationType.GET, connProperty,
                opProperty);

 

        new SimpleOperationContext(null, null, OperationType.GET, null, null,
                null, null);
        SFTPConnector connector = new SFTPConnector();

 

        ConnectorTester tester = new ConnectorTester(connector);
        Map<ObjectDefinitionRole, String> cookie = new HashMap<>();
        cookie.put(ObjectDefinitionRole.OUTPUT, "true");
        tester.setOperationContext(OperationType.GET, connProperty, opProperty, "File", cookie);
        // tester.setBrowseContext(context);
        tester.setBrowseContext(OperationType.GET, connProperty, opProperty);
        List<InputStream> streamList = new ArrayList<>();
        streamList.add(new ByteArrayInputStream("abc".getBytes("UTF-8")));

 

        Map<String, String> dynamicProperty = new HashMap<>();
        dynamicProperty.put(SFTPConstants.PROPERTY_FILENAME, "file1.txt");
        
        new SimpleTrackedData(1, new ByteArrayInputStream("abc".getBytes("UTF-8")),
                null, dynamicProperty); 
        
        tester.executeGetOperation("abc");
    }
	@Test
    public void testSFTPGetOperationBlankFileNAme() throws Exception {
        JSch jsch = mock(JSch.class);
        Session session = mock(Session.class);
        ChannelSftp channel = mock(ChannelSftp.class);

 

        PowerMockito.whenNew(JSch.class).withNoArguments().thenReturn(jsch);
        when(jsch.getSession(any(String.class), any(String.class), any(Integer.class))).thenReturn(session);
        when(session.openChannel(any(String.class))).thenReturn(channel);
        
        Map<String, Object> opProperty = new HashMap<>();
        Map<String, Object> connProperty = new HashMap<>();
        opProperty.put(SFTPConstants.PROPERTY_INCLUDE_METADATA, Boolean.TRUE);
        opProperty.put(SFTPConstants.PROPERTY_DELETE_AFTER, Boolean.TRUE); 
        connProperty.put(SFTPConstants.AUTHORIZATION_TYPE, "Using public Key");
        connProperty.put(SFTPConstants.IS_MAX_EXCHANGE, Boolean.FALSE);
        connProperty.put(SFTPConstants.USE_KEY_CONTENT, false);
        new SimpleBrowseContext(null, null, OperationType.GET, connProperty,
                opProperty);

 

        new SimpleOperationContext(null, null, OperationType.GET, null, null,
                null, null);
        SFTPConnector connector = new SFTPConnector();

 

        ConnectorTester tester = new ConnectorTester(connector);
        Map<ObjectDefinitionRole, String> cookie = new HashMap<>();
        cookie.put(ObjectDefinitionRole.OUTPUT, "true");
        tester.setOperationContext(OperationType.GET, connProperty, opProperty, "File", cookie);
        // tester.setBrowseContext(context);
        tester.setBrowseContext(OperationType.GET, connProperty, opProperty);
        List<InputStream> streamList = new ArrayList<>();
        streamList.add(new ByteArrayInputStream("abc".getBytes("UTF-8")));

 

        Map<String, String> dynamicProperty = new HashMap<>();
        dynamicProperty.put(SFTPConstants.PROPERTY_FILENAME, "file1.txt");
        
        new SimpleTrackedData(1, new ByteArrayInputStream("abc".getBytes("UTF-8")),
                null, dynamicProperty); 
        
        tester.executeGetOperation("");
    }
	
	
	@Test
    public void testSFTPGetOperationFileNotFound() throws Exception {
        JSch jsch = mock(JSch.class);
        Session session = mock(Session.class); 
        ChannelSftp channel = mock(ChannelSftp.class);
        PowerMockito.whenNew(JSch.class).withNoArguments().thenReturn(jsch);
        when(jsch.getSession(any(String.class), any(String.class), any(Integer.class))).thenReturn(session);
        when(session.openChannel(any(String.class))).thenReturn(channel);
       
        doThrow(new SftpException(2,"Error occurred")).when(channel).stat("abcde");
        Map<String, Object> opProperty = new HashMap<>();
        Map<String, Object> connProperty = new HashMap<>();
        opProperty.put(SFTPConstants.PROPERTY_INCLUDE_METADATA, Boolean.TRUE);
        opProperty.put(SFTPConstants.PROPERTY_DELETE_AFTER, Boolean.TRUE); 
        opProperty.put(SFTPConstants.PROPERTY_REMOTE_DIRECTORY, "abcde");
        connProperty.put(SFTPConstants.AUTHORIZATION_TYPE, "Using public Key");
        connProperty.put(SFTPConstants.IS_MAX_EXCHANGE, Boolean.FALSE);
        connProperty.put(SFTPConstants.USE_KEY_CONTENT, false);
        new SimpleBrowseContext(null, null, OperationType.GET, connProperty,
                opProperty);

 

       new SimpleOperationContext(null, null, OperationType.GET, null, null,
                null, null);
        SFTPConnector connector = new SFTPConnector();

 

        ConnectorTester tester = new ConnectorTester(connector);
        Map<ObjectDefinitionRole, String> cookie = new HashMap<>();
        cookie.put(ObjectDefinitionRole.OUTPUT, "true");
        tester.setOperationContext(OperationType.GET, connProperty, opProperty, "File", cookie);
        // tester.setBrowseContext(context);
        tester.setBrowseContext(OperationType.GET, connProperty, opProperty);
        List<InputStream> streamList = new ArrayList<>();
        streamList.add(new ByteArrayInputStream("abc".getBytes("UTF-8")));

 

        Map<String, String> dynamicProperty = new HashMap<>();
        dynamicProperty.put(SFTPConstants.PROPERTY_FILENAME, "file1.txt");
        
        new SimpleTrackedData(1, new ByteArrayInputStream("abc".getBytes("UTF-8")),
                null, dynamicProperty); 
        
        tester.executeGetOperation("abcd");
    }
	
	
	
}
