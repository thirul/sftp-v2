package com.boomi.connector.sftp;

import com.boomi.connector.sftp.common.PropertiesUtil;
import com.boomi.connector.sftp.common.SSHOptions;

public class ConnectionProperties {
	private String host;
	private int port;
	private AuthType authType;
	private PublicKeyParam publicKeyparam;
	private PasswordParam passwordParam;
	private final SSHOptions sshOptions;
	
	public ConnectionProperties(String host, int port, AuthType authType, PublicKeyParam publicKeyparam,
			PropertiesUtil properties) {
		this.host = host;
		this.port = port;
		this.authType = authType;
		this.publicKeyparam = publicKeyparam;
		this.sshOptions = new SSHOptions(properties);

	}
	
	public ConnectionProperties(String host, int port, AuthType authType, PasswordParam passwordParam,
			PropertiesUtil properties) {
		this.host = host;
		this.port = port;
		this.authType = authType;
		this.passwordParam = passwordParam;
		this.sshOptions = new SSHOptions(properties);

	}
	
	public String getHost() {
		return host;
	}
	
	public int getPort() {
		return port;
	}
	
	public AuthType getAuth() {
		return authType;
	}
	
	public PublicKeyParam getPubKeyParam() {
		return publicKeyparam;
	}
	
	public PasswordParam getPasswordParam() {
		return passwordParam;
	}
	
	public SSHOptions getSSHOptions() {
		return sshOptions;
	}
}
