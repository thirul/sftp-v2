//Copyright (c) 2020 Boomi, Inc.

package com.boomi.connector.sftp.common;

import com.boomi.connector.api.PropertyMap;
import com.boomi.connector.sftp.constants.SFTPConstants;

/**
 * @author Omesh Deoli
 *
 * ${tags}
 */
public class PropertiesUtil {

	private final PropertyMap connectionProperties;
	private final PropertyMap operationProperties;

	public PropertiesUtil(PropertyMap connectionProperties, PropertyMap operationProperties) {
		super();
		this.connectionProperties = connectionProperties;
		this.operationProperties = operationProperties;
	}

	public String getHostname() {
		return connectionProperties.getProperty(SFTPConstants.PROPERTY_HOST, "");
	}

	public String getUsername() {
		return connectionProperties.getProperty(SFTPConstants.PROPERTY_USERNAME, "");
	}

	public String getPassword() {
		return connectionProperties.getProperty(SFTPConstants.PROPERTY_PKEY, "");
	}

	public int getPort() {
		return connectionProperties.getLongProperty(SFTPConstants.PROPERTY_PORT, 22l).intValue();
	}

	public String getRemoteDirectory() {
		return connectionProperties.getProperty(SFTPConstants.PROPERTY_REMOTE_DIRECTORY, "");
	}

	public String getAuthType() {
		return connectionProperties.getProperty(SFTPConstants.AUTHORIZATION_TYPE, "");
	}

	public String getFileName() {
		return operationProperties.getProperty(SFTPConstants.PROPERTY_FILENAME, "");
	}

	public String getActionIfFileExists() {
		return operationProperties.getProperty(SFTPConstants.OPERATION_PROP_ACTION_IF_FILE_EXISTS, "");
	}

	public String getpassphrase() {
		return connectionProperties.getProperty(SFTPConstants.KEY_PSWRD, "");
	}

	public String getprvtkeyPath() {
		return connectionProperties.getProperty(SFTPConstants.KEY_PATH, "");
	}

	public String getTempExtension() {
		return operationProperties.getProperty(SFTPConstants.PROPERTY_TEMP_FILE_NAME, "");
	}

	public String getStagingDirectory() {
		return operationProperties.getProperty(SFTPConstants.PROPERTY_STAGING_DIRECTORY, "");
	}

	public boolean isDeleteAfterReadEnabled() {

		return operationProperties.getBooleanProperty(SFTPConstants.PROPERTY_DELETE_AFTER, false);
	}

	public String getKnownHostEntry() {

		return connectionProperties.getProperty(SFTPConstants.HOST_ENTRY, "");
	}

	public boolean isMaxExchangeEnabled() {

		return connectionProperties.getBooleanProperty(SFTPConstants.IS_MAX_EXCHANGE, false);
	}

	public String getPrivateKeyContent() {
		return connectionProperties.getProperty(
				SFTPConstants.PRIVATE_KEY_CONTENT, "");
	}

	public String getPublicKeyContent() {
		return connectionProperties.getProperty(SFTPConstants.PUBLIC_KEY_CONTENT, "");
	}
	
	public String getKeyPairName() {
		return connectionProperties.getProperty(SFTPConstants.KEY_PAIR_NAME, "");
	}
	
	public boolean isUseKeyContentEnabled() {
		return connectionProperties.getBooleanProperty(SFTPConstants.USE_KEY_CONTENT, false);
	}
}
