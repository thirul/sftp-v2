//Copyright (c) 2020 Boomi, Inc.

package com.boomi.connector.sftp.results;

import com.boomi.connector.api.FilterData;
import com.boomi.connector.api.OperationResponse;
import com.boomi.connector.api.OperationStatus;
import com.boomi.connector.api.PayloadMetadata;
import com.boomi.connector.api.TrackedData;
import com.boomi.connector.sftp.constants.SFTPConstants;

/**
 * @author Omesh Deoli
 *
 *         ${tags}
 */
public class MultiResult {

	private final OperationResponse response;
	private final FilterData input;
	private int size;

	public MultiResult(OperationResponse response, FilterData input) {
		this.response = response;
		this.input = input;
	}

	public void addPartialResult(String dirFullPath, String fileName, BaseResult result) {
		if (result.getStatus() == OperationStatus.APPLICATION_ERROR || result.getStatus() == OperationStatus.FAILURE) {

			this.addPartialErrorResult(dirFullPath, fileName, result);
		} else {
			this.addPartialSuccessResult(dirFullPath, fileName, result);
		}
	}

	public void addPartialResult(ErrorResult e) {
		this.response.addPartialResult((TrackedData) this.input, e.getStatus(), e.getStatusCode(),
				e.getStatusMessage(), null);
		++this.size;
	}
	private void addPartialErrorResult(String dirFullPath, String fileName, BaseResult result) {
		PayloadMetadata metadata = response.createMetadata();
		metadata.setTrackedProperty(SFTPConstants.PROPERTY_FILENAME, fileName);
		metadata.setTrackedProperty(SFTPConstants.REMOTE_DIRECTORY, dirFullPath);
		this.response.addPartialResult((TrackedData) this.input, result.getStatus(), result.getStatusCode(),
				"Error getting file " + fileName, null);
		++this.size;

	}

	public void addPartialSuccessResult(String dirFullPath, String filename, BaseResult result) {
		PayloadMetadata metadata = response.createMetadata();
		metadata.setTrackedProperty(SFTPConstants.PROPERTY_FILENAME, filename);
		metadata.setTrackedProperty(SFTPConstants.REMOTE_DIRECTORY, dirFullPath);
		this.addPartial(result);
		++this.size;
	}

	private void addPartial(BaseResult result) {
		this.response.addPartialResult((TrackedData) this.input, result.getStatus(), result.getStatusCode(),
				result.getStatusMessage(), result.getPayload());
	}

	public void finish() {
		this.response.finishPartialResult((TrackedData) this.input);

	}

	public FilterData getInput() {
		return this.input;
	}

	public int getSize() {
		return this.size;
	}

	public boolean isEmpty() {
		return this.size == 0;
	}
}
