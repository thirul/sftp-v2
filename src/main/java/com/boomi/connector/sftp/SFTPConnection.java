//Copyright (c) 2020 Boomi, Inc.

package com.boomi.connector.sftp;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;

import com.boomi.connector.api.TrackedData;
import com.boomi.connector.sftp.actions.RetryableGetFileMetadataAction;
import com.boomi.connector.sftp.common.ExtendedSFTPFileMetadata;
import com.boomi.connector.sftp.common.MeteredTempOutputStream;
import com.boomi.connector.sftp.common.PathsHandler;
import com.boomi.connector.sftp.common.PropertiesUtil;
import com.boomi.connector.sftp.common.SFTPFileMetadata;
import com.boomi.connector.sftp.common.UnixPathsHandler;
import com.boomi.connector.sftp.constants.SFTPConstants;
import com.boomi.connector.sftp.results.EmptySuccess;
import com.boomi.connector.sftp.results.Result;
import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.ConnectorException;
import com.boomi.connector.api.FilterData;
import com.boomi.connector.api.ObjectIdData;
import com.boomi.connector.util.BaseConnection;
import com.boomi.util.IOUtil;
import com.boomi.util.StreamUtil;
import com.boomi.util.StringUtil;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.ChannelSftp.LsEntrySelector;
import com.jcraft.jsch.SftpATTRS;

/**
 * @author Omesh Deoli
 *
 *         ${tags}
 */
@SuppressWarnings("rawtypes")
public class SFTPConnection extends BaseConnection {

	private final PathsHandler pathsHandler;

	private SFTPClient client;

	public SFTPClient getClient() {
		return client;
	}

	private final String remoteDirectory;

	public PathsHandler getPathsHandler() {
		return pathsHandler;
	}

	@SuppressWarnings("unchecked")
	public SFTPConnection(BrowseContext context) {
		super(context);
		this.pathsHandler = new UnixPathsHandler();
		this.remoteDirectory = StringUtil
				.trim(context.getConnectionProperties().getProperty(SFTPConstants.REMOTE_DIRECTORY));

	}

	public void openConnection() {
		this.client = createSftpClient(new PropertiesUtil(this.getContext().getConnectionProperties(), null));
		this.client.openConnection();
	}

	public SFTPClient createSftpClient(PropertiesUtil propertiesUtil) {

		if (propertiesUtil.getAuthType().equals(AuthType.PUBLIC_KEY.getAuthType())) {
			PublicKeyParam param;
			if (!propertiesUtil.isUseKeyContentEnabled()) {
				param = new PublicKeyParam(propertiesUtil.getpassphrase(), propertiesUtil.getprvtkeyPath(),
						propertiesUtil.getUsername(), propertiesUtil.isUseKeyContentEnabled());
			} else {
				param = new PublicKeyParam(propertiesUtil.getUsername(), propertiesUtil.getpassphrase(),
						propertiesUtil.getPrivateKeyContent(), propertiesUtil.getPublicKeyContent(),
						propertiesUtil.getKeyPairName(), propertiesUtil.isUseKeyContentEnabled());
			}
			return new SFTPClient(propertiesUtil.getHostname(), propertiesUtil.getPort(), AuthType.PUBLIC_KEY, param,
					propertiesUtil);
		} else {
			PasswordParam passwordParam = new PasswordParam(propertiesUtil.getUsername(), propertiesUtil.getPassword());
			return new SFTPClient(propertiesUtil.getHostname(), propertiesUtil.getPort(), AuthType.PASSWORD,
					passwordParam, propertiesUtil);
		}
	}

	public boolean isConnected() {
		return this.client.isConnected();
	}

	public void closeConnection() {
		client.closeConnection();

	}

	@SuppressWarnings("deprecation")
	public String getDocOrOperationProperty(TrackedData input, String propName) {
		String propertyValue = SFTPUtil.getDocProperty(input, propName);
		if (StringUtil.isNotBlank((String) propertyValue)) {
			return propertyValue;
		}
		return StringUtil.trim((String) this.getOperationContext().getOperationProperties().getProperty(propName));
	}

	public void renameFile(String fullPath, String newFullPath) {

		client.renameFile(fullPath, newFullPath);

	}

	public void putFile(InputStream content, String filePath, int mode) {

		client.putFile(content, filePath, mode);

	}

	public boolean isFilePresent(String filePath) {

		return client.isFilePresent(filePath);
	}

	public void deleteFile(String filePath) {

		client.deleteFile(filePath);

	}

	public Result deleteFile(ObjectIdData input) {
		String fileFullPath = getFileFullPath(input);
		deleteFile(fileFullPath);
		return new EmptySuccess();
	}

	private String getFileFullPath(ObjectIdData input) {

		String enteredFilePath = input.getObjectId();
		if (StringUtil.isBlank((String) enteredFilePath)) {
			throw new ConnectorException(SFTPConstants.ERROR_MISSING_INPUT_FILENAME);
		}

		String enteredRemoteDir = getEnteredRemoteDirectory(input);
		String remoteDir;
		if (StringUtil.isBlank(enteredRemoteDir) || !pathsHandler.isFullPath(enteredRemoteDir)) {
			 input.getLogger().log(Level.INFO,
						"Entered remote directory path is blank or not a absolute full path.Setting home directory of user as default working path");
			remoteDir = pathsHandler.resolvePaths(client.getHomeDirectory(), enteredRemoteDir);
		} else {
			remoteDir = enteredRemoteDir;
		}

		return pathsHandler.joinPaths(remoteDir, enteredFilePath);

	}

	public InputStream getFileStream(String fullFilePath) {

		return client.getFileStream(fullFilePath);

	}

	public void closeStream(InputStream stream) {
		IOUtil.closeQuietly((Closeable[]) new Closeable[] { stream });
	}

	public void createDirectory(String filePath) {
		client.createDirectory(filePath);

	}

	public void createNestedDirectories(String fullPath) {
		client.createNestedDirectory(fullPath);

	}

	public String getCurrentDirectory() {

		return client.getCurrentDirectory();

	}

	public String getEnteredRemoteDirectory(TrackedData input) {

		String dirPath = SFTPUtil.getDocProperty(input, SFTPConstants.REMOTE_DIRECTORY);
		return StringUtil.defaultIfBlank((String) dirPath, (String) this.remoteDirectory);
	}

	public void changeCurrentDirectory(String remoteDir) {
		client.changeCurrentDirectory(remoteDir);

	}
	
	public String findUniqueFileName(String enteredFileName, String remoteDir) {
		return client.getUniqueFileName(remoteDir, enteredFileName);
	}

	public boolean fileExists(String enteredFileName, String remoteDir) {

		return client.isFilePresent(pathsHandler.joinPaths(remoteDir, enteredFileName));
	}

	public boolean fileExists(String fullPath) {

		return client.isFilePresent(fullPath);
	}

	public ExtendedSFTPFileMetadata getFileMetadata(String dirFullPath, String fileName) {

		RetryableGetFileMetadataAction retryableMetadataaction = new RetryableGetFileMetadataAction(this, dirFullPath,
				fileName);
		retryableMetadataaction.execute();
		String formattedDate = SFTPUtil
				.formatDate(SFTPUtil.parseDate(retryableMetadataaction.get_fileMetaData().getMTime()));

		return new ExtendedSFTPFileMetadata(dirFullPath, fileName, formattedDate);
	}

	public FileQueryFilter makeFilter(FilterData input, boolean filesOnly, File dirFullPath, String dirPath) {
		if (filesOnly) {
			return new FileExclusiveQueryFilter(dirFullPath, input, dirPath);
		}
		return new FileQueryFilter(dirFullPath, input, dirPath);
	}

	public long getLimit() {
		@SuppressWarnings("deprecation")
		long limit = this.getOperationContext().getOperationProperties().getLongProperty(SFTPConstants.COUNT);
		if (limit == -1L) {
			return Long.MAX_VALUE;
		}
		if (limit <= 0L) {
			throw new IllegalArgumentException(String.format(SFTPConstants.LIMIT_MUST_BE_POSITIVE, limit));
		}
		return limit;
	}

	public String getHomeDirectory() {

		return client.getHomeDirectory();
	}

	public void reconnect() {
		closeConnection();
		this.openConnection();
	}

	public void uploadFile(String filePath, InputStream inputStream, long appendOffset) {
		long actualTransferredBytes;
		SFTPFileMetadata ftpFileMetadata = this.pathsHandler.splitIntoDirAndFileName(filePath);
		actualTransferredBytes = this.getSizeOnRemote(ftpFileMetadata.getDirectory(), ftpFileMetadata.getName());
		if (appendOffset > 0L) {
			actualTransferredBytes -= appendOffset;
		}

		SFTPConnection.adjustInputStreamToResumeUpload(inputStream, actualTransferredBytes);

		this.client.putFile(inputStream, filePath, ChannelSftp.APPEND);

	}

	private static void adjustInputStreamToResumeUpload(InputStream inputStream, long transferredBytes) {
		try {
			inputStream.reset();
			StreamUtil.skipFully(inputStream, transferredBytes);
		} catch (IOException e) {
			throw new ConnectorException(SFTPConstants.ERROR_RESUMING_UPLOAD, e);
		}
	}

	public long getSizeOnRemote(String directory, String name) {
		boolean filePresent = client.isFilePresent(pathsHandler.joinPaths(directory, name));
		if (filePresent) {
			return client.getFileMetadata(pathsHandler.joinPaths(directory, name)).getSize();
		} else {
			return 0L;
		}
	}

	public void getFile(String fileName, MeteredTempOutputStream outputStream) {
		long noOfBytestoskip = outputStream.getCount();
		this.client.retrieveFile(fileName, outputStream, noOfBytestoskip);

	}

	public void getFile(String fileName, MeteredTempOutputStream outputStream, SFTPClient client) {
		long noOfBytestoskip = outputStream.getCount();
		client.retrieveFile(fileName, outputStream, noOfBytestoskip);

	}

	public SftpATTRS getSingleFileAttributes(String remoteDir, String fileName) {
		return client.getFileMetadata(pathsHandler.joinPaths(remoteDir, fileName));

	}

	public void deleteFile(String remoteDir, String fileName) {
		client.deleteFile(pathsHandler.joinPaths(remoteDir, fileName));

	}

	public void listDirectoryContentWithSelector(String dirfullPath, LsEntrySelector selector) {
		client.listDirectoryContentWithSelector(dirfullPath, selector);
	}

	public void testConnection() {
		openConnection();
		closeConnection();
	}
}
