//Copyright (c) 2020 Boomi, Inc.

package com.boomi.connector.sftp;

import java.io.Closeable;
import java.io.InputStream;
import java.text.MessageFormat;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.boomi.connector.api.ConnectorException;
import com.boomi.connector.sftp.common.MeteredTempOutputStream;
import com.boomi.connector.sftp.common.PropertiesUtil;
import com.boomi.connector.sftp.constants.SFTPConstants;
import com.boomi.connector.sftp.exception.NoSuchFileFoundException;
import com.boomi.connector.sftp.exception.SFTPSdkException;
import com.boomi.util.StringUtil;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.ChannelSftp.LsEntry;
import com.jcraft.jsch.ChannelSftp.LsEntrySelector;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpATTRS;
import com.jcraft.jsch.SftpException;

/**
 * @author Omesh Deoli
 *
 *         ${tags}
 */
public class SFTPClient implements Closeable {

	private ChannelSftp sftpChannel;
	private ConnectionProperties conProp;
	private Session session;
	private String retFileName = null;
	private int counter;
	private boolean filePresence = false;
	private PropertiesUtil properties;

	private Logger logger = Logger.getLogger(SFTPClient.class.getName());

	public SFTPClient(String host, int port, AuthType authType, PublicKeyParam publicKeyparam,
			PropertiesUtil properties) {
		conProp = new ConnectionProperties(host, port, authType, publicKeyparam, properties);
		this.properties = properties;
	}

	public SFTPClient(String host, int port, AuthType authType, PasswordParam passwordParam,
			PropertiesUtil properties) {
		conProp = new ConnectionProperties(host, port, authType, passwordParam, properties);
		this.properties = properties;
	}

	public void openConnection() {
		if (session == null || !session.isConnected()) {
			getSession();
		}
		if (sftpChannel == null || !sftpChannel.isConnected()) {
			getChannel();
		}
	}

	private void getSession() {
		try {
			session = StackSessionPool.getInstance().getPool().borrowObject(conProp);
		} catch (Exception e) {
			returnSession();
			validateNullFields();
			if (e.getLocalizedMessage().contains(SFTPConstants.FILE_NOT_FOUND_EXCEPTION)) {
				throw new ConnectorException(SFTPConstants.FILE_NOT_FOUND_MESSAGE);
			} else if (e.getLocalizedMessage().contains(SFTPConstants.INVALID_PRIVATEKEY)) {
				throw new ConnectorException(SFTPConstants.INVALID_PRIVATEKEY_ERROR_MESSAGE);
			} else if (e.getLocalizedMessage().contains(SFTPConstants.ACCESS_DENIED)) {
				throw new ConnectorException(SFTPConstants.ACCESS_DENIED_ERROR_MESSAGE);
			} else if (e.getLocalizedMessage().contains(SFTPConstants.AUTH_FAIL)) {
					throw new ConnectorException(SFTPConstants.AUTH_FAIL_ERROR_MESSAGE);
			} else if (e.getLocalizedMessage().contains(SFTPConstants.USER_AUTH_FAIL)) {
				throw new ConnectorException(SFTPConstants.USER_AUTH_FAIL_MESSAGE);
			} else if (e.getLocalizedMessage().contains(SFTPConstants.PORT)) {
				throw new ConnectorException(SFTPConstants.PORT_ERROR_MESSAGE);
			} else if (e.getLocalizedMessage().contains(SFTPConstants.UNKNOWN_HOST)) {
				throw new ConnectorException(SFTPConstants.UNKNOWN_HOST_ERROR_MESSAGE);
			} else {
				throw new ConnectorException("Unable to get Session", e.getMessage());
			}
		}

	}

	/**
	 * Method to check if the fields are empty or null and throw customized error
	 * messages.
	 */
	private void validateNullFields() {

		if (StringUtil.isBlank(properties.getHostname())) {
			throw new ConnectorException(SFTPConstants.HOSTNAME_BLANK);
		}
		if (StringUtil.isBlank(properties.getUsername())) {
			throw new ConnectorException(SFTPConstants.USER_BLANK);
		} 
		if ("Username and Password".equals(conProp.getAuth().getAuthType())) {			
			if (StringUtil.isBlank(properties.getPassword())) {
				throw new ConnectorException(SFTPConstants.PASS_BLANK);
			}
		} else {
			if (properties.isUseKeyContentEnabled()) {
				if (StringUtil.isBlank(properties.getPrivateKeyContent())) {
					throw new ConnectorException(SFTPConstants.PRIVATE_KEY_BLANK);
				} 
				if (StringUtil.isBlank(properties.getPublicKeyContent())) {
					throw new ConnectorException(SFTPConstants.PUBLIC_KEY_BLANK);
				}
			} else {
				if (StringUtil.isBlank(properties.getprvtkeyPath())) {
                    throw new ConnectorException(SFTPConstants.KEY_FILE_PATH);
				}
			}
			
		}

	}

	public void returnSession() {
		if (session != null) {
			try {
				StackSessionPool.getInstance().getPool().returnObject(conProp, session);
			} catch (Exception e) {
				throw new ConnectorException("Unable to return Session", e.getLocalizedMessage());
			}
		}
	}

	public void closeConnection() {
		killChannel();
		returnSession();
	}

	private void killChannel() {
		if (this.sftpChannel != null && this.sftpChannel.isConnected()) {
			try {
				this.logger.fine(SFTPConstants.DISCONNECTING_FROM_SFTP_SERVER);
				this.sftpChannel.disconnect();
			} catch (Exception e) {
				this.logger.log(Level.FINE, SFTPConstants.ERROR_DISCONNECTING_CHANNEL, e);
			}
		}
	}

	public void getChannel() {
		try {
			Channel channel = session.openChannel(SFTPConstants.SFTP);
			channel.connect();
			this.sftpChannel = (ChannelSftp) channel;
		} catch (JSchException e) {
			closeConnection();
			throw new ConnectorException(SFTPConstants.ERROR_FAILED_OPENING_SFTP_CHANNEL, e);
		}
	}

	public void putFile(InputStream content, String filePath, int mode) {

		try {
			sftpChannel.put(content, filePath, mode);
		} catch (SftpException e) {
			throw new SFTPSdkException(MessageFormat.format(SFTPConstants.ERROR_FAILED_FILE_UPLOAD, filePath)
					+ SFTPConstants.CAUSE + e.getMessage(), e);
		}

	}

	public void renameFile(String filePath, String newFilePath) {

		try {
			sftpChannel.rename(filePath, newFilePath);
		} catch (SftpException e) {
			throw new SFTPSdkException(MessageFormat.format(SFTPConstants.ERROR_FAILED_RENAME, filePath, newFilePath)
					+ SFTPConstants.CAUSE + e.getMessage(), e);
		}
	}

	public boolean isFilePresent(String filePath) {

		try {
			sftpChannel.stat(filePath);
			return true;
		} catch (SftpException e) {
			if (e.id == ChannelSftp.SSH_FX_NO_SUCH_FILE)
				return false;
			else {
				throw new SFTPSdkException(
						MessageFormat.format(SFTPConstants.ERROR_FAILED_CHECKING_FILE_EXISTENCE, filePath)
								+ SFTPConstants.CAUSE + e.getMessage(),
						e);
			}
		}
	}

	public void deleteFile(String filePath) {

		try {
			sftpChannel.rm(filePath);
		} catch (SftpException e) {
			if (e.id == ChannelSftp.SSH_FX_NO_SUCH_FILE) {
				throw new NoSuchFileFoundException(SFTPConstants.FILE_NOT_FOUND, e);
			}
			throw new SFTPSdkException(MessageFormat.format(SFTPConstants.ERROR_FAILED_FILE_REMOVAL, filePath)
					+ SFTPConstants.CAUSE + e.getMessage(), e);
		}

	}

	public InputStream getFileStream(String filePath) {

		try {
			return sftpChannel.get(filePath);
		} catch (SftpException e) {
			if (e.id == ChannelSftp.SSH_FX_NO_SUCH_FILE) {
				throw new NoSuchFileFoundException(SFTPConstants.FILE_NOT_FOUND, e);
			}
			throw new SFTPSdkException(MessageFormat.format(SFTPConstants.ERROR_FAILED_FILE_RETRIEVAL, filePath)
					+ SFTPConstants.CAUSE + e.getMessage(), e);
		}

	}

	static boolean canRetrieve(ChannelSftp.LsEntry entry) {
		return !entry.getAttrs().isDir() && !entry.getAttrs().isLink()
				&& !StringUtil.isBlank((String) entry.getFilename());
	}

	boolean isNumeric(String str) {
		for (int i = 0; i < str.length(); i++) {
			if (str.charAt(i) > '9' || str.charAt(i) < '0')
				return false;
		}
		return true;
	}

	public String getUniqueFileName(String path, String inputfileName) {
		final String inputFileName = inputfileName;
		final int inpFileNameLength = inputFileName.indexOf('.');
		counter = 0;
		filePresence = false;
		retFileName = inputfileName;
		LsEntrySelector selector = new LsEntrySelector() {
			public int select(LsEntry entry) {
				final String filename = entry.getFilename();
				if (filename.equals(".") || filename.equals("..")) {
					return CONTINUE;
				}
				if (entry.getAttrs().isLink() || entry.getAttrs().isDir()) {
					return CONTINUE;
				}
				counter++;
				if (filePresence == false && inputFileName.equals(filename)) {
					String ienteredFileName = inputFileName.substring(0, inpFileNameLength);
					String ext = inputFileName.substring(inputFileName.lastIndexOf('.'), inputFileName.length());
					retFileName = ienteredFileName.concat("1").concat(ext);
					filePresence = true;
				} else {
					if (filename.length() > inputFileName.length() && filePresence && retFileName.equals(filename)) {
						String ifilename = filename.substring(0, inpFileNameLength);
						String ienteredFileName = inputFileName.substring(0, inpFileNameLength);
						String subString = filename.substring(inpFileNameLength, filename.lastIndexOf('.'));
						String ext = filename.substring(filename.lastIndexOf('.'), filename.length());
						if (ifilename.equals(ienteredFileName) && isNumeric(subString)) {
							int append = Integer.parseInt(subString);
							retFileName = ienteredFileName.concat(String.valueOf(append + 1)).concat(ext);
						}
					}
				}
				return CONTINUE;
			}
		};
		try {
			// here we are going to iterate over file names N^2 times

			sftpChannel.ls(path, selector);

			int tempValue = counter - 1;
			// because we iterated once, tempValue = GlobalVariables.getNumberOfFiles() - 1

			while (tempValue > 0) {
				sftpChannel.ls(path, selector);
				tempValue--;
			}
		} catch (SftpException e) {
			throw new SFTPSdkException(
					SFTPConstants.ERROR_FAILED_LISTING_FILENAMES + SFTPConstants.CAUSE + e.getMessage(), e);
		}
		return retFileName;
	}

	public void createDirectory(String filePath) {
		try {
			sftpChannel.mkdir(filePath);
		} catch (SftpException e) {
			throw new SFTPSdkException(MessageFormat.format(SFTPConstants.ERROR_FAILED_DIRECTORY_CREATE, filePath)
					+ SFTPConstants.CAUSE + e.getMessage(), e);
		}
	}

	public String getCurrentDirectory() {
		try {
			return sftpChannel.pwd();
		} catch (SftpException e) {

			throw new SFTPSdkException(
					SFTPConstants.ERROR_FAILED_RETRIEVING_DIRECTORY + SFTPConstants.CAUSE + e.getMessage(), e);
		}

	}

	public boolean isConnected() {

		return sftpChannel != null && sftpChannel.isConnected() && !sftpChannel.isClosed() && session != null
				&& session.isConnected();

	}

	public void changeCurrentDirectory(String fullPath) {
		try {
			sftpChannel.cd(fullPath);
		} catch (SftpException e) {
			throw new SFTPSdkException(
					MessageFormat.format(SFTPConstants.ERROR_FAILED_CHANGING_CURRENT_DIRECTORY, fullPath)
							+ SFTPConstants.CAUSE + e.getMessage(),
					e);
		}
	}

	public SftpATTRS getFileMetadata(String fileFullPath) {

		try {
			return sftpChannel.lstat(fileFullPath);
		} catch (SftpException e) {
			throw new SFTPSdkException(
					MessageFormat.format(SFTPConstants.ERROR_FAILED_GETING_FILE_METADATA, fileFullPath)
							+ SFTPConstants.CAUSE + e.getMessage(),
					e);

		}
	}

	public void createNestedDirectory(String fullPath) {
		String[] folders = fullPath.split("/");
		changeCurrentDirectory("/");
		for (String folder : folders) {
			if (folder.length() > 0) {
				try {
					changeCurrentDirectory(folder);
				} catch (SFTPSdkException e) {
					createDirectory(folder);
					changeCurrentDirectory(folder);
				}
			}
		}
	}

	public String getHomeDirectory() {
		try {
			return sftpChannel.getHome();
		} catch (SftpException e) {
			throw new SFTPSdkException(SFTPConstants.ERROR_GETTING_HOME_DIR + SFTPConstants.CAUSE + e.getMessage());
		}
	}

	public void retrieveFile(String fileName, MeteredTempOutputStream outputStream, long noOfBytestoSkip) {

		try {
			sftpChannel.get(fileName, outputStream, null, ChannelSftp.RESUME, noOfBytestoSkip);
		} catch (SftpException e) {
			if (e.id == ChannelSftp.SSH_FX_NO_SUCH_FILE) {
				throw new NoSuchFileFoundException(SFTPConstants.FILE_NOT_FOUND, e);
			}
			throw new SFTPSdkException(MessageFormat.format(SFTPConstants.ERROR_FAILED_FILE_RETRIEVAL, fileName)
					+ SFTPConstants.CAUSE + e.getMessage(), e);
		}

	}

	public void listDirectoryContentWithSelector(String dirfullPath, LsEntrySelector selector) {
		try {
			sftpChannel.ls(dirfullPath, selector);

		} catch (SftpException e) {
			throw new SFTPSdkException(
					MessageFormat.format(SFTPConstants.ERROR_FAILED_GETING_DIRECTORY_CONTENTS, dirfullPath)
							+ SFTPConstants.CAUSE + e.getMessage(),
					e);
		}
	}

	@Override
	public void close() {
		closeConnection();

	}
}
