//Copyright (c) 2020 Boomi, Inc.

package com.boomi.connector.sftp.operations;

import com.boomi.connector.api.FilterData;
import com.boomi.connector.api.OperationResponse;
import com.boomi.connector.api.OperationStatus;
import com.boomi.connector.api.QueryRequest;
import com.boomi.connector.sftp.ListResultBuilder;
import com.boomi.connector.sftp.QueryResultBuilder;
import com.boomi.connector.sftp.ResultBuilder;
import com.boomi.connector.sftp.SFTPConnection;
import com.boomi.connector.sftp.SFTPCustomType;
import com.boomi.connector.sftp.constants.SFTPConstants;
import com.boomi.connector.sftp.handlers.QueryHandler;
import com.boomi.connector.sftp.results.ErrorResult;
import com.boomi.connector.sftp.results.MultiResult;
import com.boomi.connector.util.BaseConnection;
import com.boomi.connector.util.BaseQueryOperation;
import com.boomi.util.LogUtil;
import java.util.logging.Logger;

/**
 * @author Omesh Deoli
 *
 * ${tags}
 */
public class SFTPQueryOperation extends BaseQueryOperation {
	public SFTPQueryOperation(SFTPConnection conn) {
		super((BaseConnection<?>) conn);
	}

	protected void executeQuery(QueryRequest queryRequest, OperationResponse operationResponse) {
		SFTPConnection conn = this.getConnection();
		FilterData input = queryRequest.getFilter();
		MultiResult multiResult = new MultiResult(operationResponse, input);
		try {
			conn.openConnection();
			SFTPCustomType type = SFTPCustomType.valueOf(this.getContext().getCustomOperationType());
			QueryHandler qhandler= new QueryHandler(conn);
			switch (type) {
			case LIST: {
				ResultBuilder resultBuilder=new ListResultBuilder();
				qhandler.doQuery(multiResult, false, resultBuilder);
				return;
			}
			case QUERY: {
				ResultBuilder resultBuilder=new QueryResultBuilder();
				qhandler.doQuery(multiResult, true, resultBuilder);
				return;
			}
			default: {
				throw new IllegalArgumentException(SFTPConstants.INVALID_QUERY_TYPE + type);
			}
			}
		} catch (Exception e) {
			LogUtil.severe((Logger) input.getLogger(), SFTPConstants.ERROR_SEARCHING, e);
			OperationStatus status = multiResult.isEmpty() ? OperationStatus.FAILURE
					: OperationStatus.APPLICATION_ERROR;
			multiResult.addPartialResult(new ErrorResult(status, e));

		} finally {
			multiResult.finish();
			conn.closeConnection();
		}
	}

	@Override
	public SFTPConnection getConnection() {
		return (SFTPConnection) super.getConnection();
	}

}
