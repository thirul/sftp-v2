//Copyright (c) 2020 Boomi, Inc.

package com.boomi.connector.sftp;


import com.jcraft.jsch.DHGEX;

/**
 * @author Omesh Deoli
 *
 * ${tags}
 */
public class DHGEX1024
extends DHGEX {
    private static final int MAX_KEYSIZE = 1024;

    protected int check2048(@SuppressWarnings("rawtypes") Class c, int _max) throws Exception {
        return _max > MAX_KEYSIZE ? MAX_KEYSIZE : _max;
    }
}

