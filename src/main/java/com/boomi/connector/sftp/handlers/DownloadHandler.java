//Copyright (c) 2020 Boomi, Inc.

package com.boomi.connector.sftp.handlers;

import java.io.InputStream;
import java.util.logging.Level;

import com.boomi.connector.api.ConnectorException;
import com.boomi.connector.api.ObjectIdData;
import com.boomi.connector.api.OperationResponse;
import com.boomi.connector.api.OperationStatus;
import com.boomi.connector.api.PayloadUtil;
import com.boomi.connector.api.PropertyMap;
import com.boomi.connector.api.TrackedData;
import com.boomi.connector.sftp.SFTPConnection;
import com.boomi.connector.sftp.actions.RetryableDeleteFileAtPathAction;
import com.boomi.connector.sftp.actions.RetryableRetrieveFileAction;
import com.boomi.connector.sftp.common.PathsHandler;
import com.boomi.connector.sftp.common.SFTPFileMetadata;
import com.boomi.connector.sftp.constants.SFTPConstants;
import com.boomi.connector.sftp.exception.NoSuchFileFoundException;
import com.boomi.connector.sftp.exception.SFTPSdkException;
import com.boomi.connector.sftp.results.BaseResult;
import com.boomi.connector.sftp.results.EmptySuccess;
import com.boomi.connector.sftp.results.ErrorResult;
import com.boomi.connector.sftp.results.Result;
import com.boomi.connector.sftp.retry.RetryStrategyFactory;
import com.boomi.util.StringUtil;

/**
 * @author Omesh Deoli
 *
 *         ${tags}
 */
public class DownloadHandler {

	final SFTPConnection connection;
	final OperationResponse operationResponse;

	final PathsHandler pathsHandler;
	Boolean isdeleteAfterEnabled;
	Boolean isfailDeleteAfter;
	SFTPFileMetadata fileMetadata;
	private final RetryStrategyFactory downloadRetryFactory;

	public DownloadHandler(SFTPConnection connection, OperationResponse operationResponse) {

		this.connection = connection;
		this.operationResponse = operationResponse;
		this.pathsHandler = this.connection.getPathsHandler();
		@SuppressWarnings("deprecation")
		PropertyMap opProperties = this.connection.getOperationContext().getOperationProperties();
		isdeleteAfterEnabled = opProperties.getBooleanProperty(SFTPConstants.PROPERTY_DELETE_AFTER);
		isfailDeleteAfter = opProperties.getBooleanProperty(SFTPConstants.PROPERTY_FAIL_DELETE_AFTER);
		this.downloadRetryFactory = RetryStrategyFactory.createFactory(1);
	}

	public void processInput(ObjectIdData input) {

		Result res = null;
		InputStream inputStream = null;
		DownloadPaths downloadPaths = this.getAndValidateNormalizedPaths((TrackedData) input);
		RetryableRetrieveFileAction getFileAction = null;
		try {
			String fullFilePath = pathsHandler.joinPaths(downloadPaths.getRemoteDirFullPath(), fileMetadata.getName());
			getFileAction = new RetryableRetrieveFileAction(connection, downloadPaths.getRemoteDirFullPath(),
					fullFilePath, input, downloadRetryFactory);

			getFileAction.execute();
			inputStream = getFileAction.get_outputStream().toInputStream();
			if (isdeleteAfterEnabled) {
				res = deleteFileInRemoteDir(fullFilePath, input, res, inputStream);
				if (res != null) {
					res.addToResponse(operationResponse, (TrackedData) input);
					return;
				}

			}
			res = new BaseResult(PayloadUtil.toPayload(inputStream));
			res.addToResponse(operationResponse, (TrackedData) input);
		} catch (NoSuchFileFoundException e) {
			res = new EmptySuccess();
			res.addToResponse(operationResponse, (TrackedData) input);
		} catch (Exception e) {
			res = new ErrorResult(e);
			res.addToResponse(operationResponse, (TrackedData) input);
		} finally {
			connection.closeStream(inputStream);
			if (getFileAction != null) {
				getFileAction.close();
			}
		}
	}

	private Result deleteFileInRemoteDir(String fullFilePath, ObjectIdData input, Result res, InputStream fileContent) {
		try {
			RetryableDeleteFileAtPathAction deleteAtPath = new RetryableDeleteFileAtPathAction(connection, fullFilePath,
					input);
			deleteAtPath.execute();

		} catch (Exception e) {
			// log a warning message delete failure
			input.getLogger().log(Level.WARNING, SFTPConstants.UNABLE_TO_DELETE_FILE);
			// check if "Fail if unable to delete" checkbox is selected in operation tab
			// if selected, then mark the operation status as application error.
			if (isfailDeleteAfter) {
				res = new BaseResult(PayloadUtil.toPayload(fileContent), "-1", SFTPConstants.UNABLE_TO_DELETE_FILE,
						OperationStatus.APPLICATION_ERROR);

			}
		}
		return res;
	}

	private static final class DownloadPaths {

		private final String remoteDirFullPath;

		DownloadPaths(String remoteDirFullPath) {
			this.remoteDirFullPath = remoteDirFullPath;

		}

		String getRemoteDirFullPath() {
			return this.remoteDirFullPath;
		}

	}

	private DownloadPaths getAndValidateNormalizedPaths(TrackedData input) {
		String enteredFilePath = ((ObjectIdData) input).getObjectId();

		if (StringUtil.isBlank(enteredFilePath)) {
			throw new ConnectorException(SFTPConstants.ERROR_MISSING_INPUT_FILENAME_ID);
		}
		fileMetadata = this.extractRemoteDirAndFileName(input, enteredFilePath);
		String remoteDir = fileMetadata.getDirectory();

		return new DownloadPaths(remoteDir);
	}

	SFTPFileMetadata extractRemoteDirAndFileName(ObjectIdData input) {
		return this.extractRemoteDirAndFileName((TrackedData) input, input.getObjectId());
	}

	String toFullPath(String childPath) {
		return this.pathsHandler.resolvePaths(connection.getHomeDirectory(), childPath);
	}

	SFTPFileMetadata extractRemoteDirAndFileName(TrackedData input, String filePath) {
		String enteredRemoteDir = this.connection.getEnteredRemoteDirectory(input);
		String remoteDir;
		if (StringUtil.isBlank(enteredRemoteDir) || !pathsHandler.isFullPath(enteredRemoteDir)) {
			 input.getLogger().log(Level.INFO,
						"Entered remote directory path is blank or not a absolute full path.Setting home directory of user as default working path");
			remoteDir = this.toFullPath(enteredRemoteDir);
		} else {
			remoteDir = enteredRemoteDir;
		}
		boolean fileExists = this.connection.fileExists(remoteDir);
		SFTPFileMetadata fileMetaData = this.pathsHandler.splitIntoDirAndFileName(remoteDir, filePath);
		if (!fileExists) {
			throw new SFTPSdkException(SFTPConstants.ERROR_REMOTE_DIRECTORY_NOT_FOUND);
		}
		return fileMetaData;
	}
}
