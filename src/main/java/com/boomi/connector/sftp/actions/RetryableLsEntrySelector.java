//Copyright (c) 2020 Boomi, Inc.

package com.boomi.connector.sftp.actions;

import java.util.logging.Level;

import com.boomi.connector.api.TrackedData;
import com.boomi.connector.sftp.CustomLsEntrySelector;
import com.boomi.connector.sftp.SFTPConnection;
import com.boomi.connector.sftp.exception.SFTPSdkException;
import com.jcraft.jsch.ChannelSftp.LsEntrySelector;

/**
 * @author Omesh Deoli
 *
 *         ${tags}
 */
public class RetryableLsEntrySelector {

	private final LsEntrySelector selector;
	private String dirFullPath;
	private TrackedData input;
	private SFTPConnection connection;

	public RetryableLsEntrySelector(SFTPConnection connection, String remoteDir, TrackedData input,
			LsEntrySelector selector) {
		this.selector = selector;
		this.dirFullPath = remoteDir;
		this.input = input;
		this.connection = connection;
	}

	public LsEntrySelector getSelector() {
		return selector;
	}

	public void execute() {

		try {
			this.doExecute();

		} catch (SFTPSdkException e) {
			if (e.getCause() != null && e.getCause().getCause() != null && e.getCause().getCause().getCause() != null
					&& e.getCause().getCause().getCause().toString().contains("DataStoreLimitException")) {
				throw e;
			}

			CustomLsEntrySelector customselctor = ((CustomLsEntrySelector) selector);
			if (customselctor.isReconnectFailed()) {

				this.input.getLogger().log(Level.WARNING,
						"Failed to re-establish connectivity with remote server after maximum allowed attempts. Could not fetch all the documents");
				return;
			}
			if(!customselctor.isReconnectFailed()) {
				this.input.getLogger().log(Level.WARNING,
						"Re-established connectivity after failure.Successfully fetched all the documents.");
				return;
			}
			throw e;

		}
	}

	public void doExecute() {
		connection.listDirectoryContentWithSelector(dirFullPath, selector);
	}

}
