//Copyright (c) 2020 Boomi, Inc.

package com.boomi.connector.sftp.actions;

import com.boomi.connector.api.TrackedData;

import com.boomi.connector.sftp.SFTPConnection;

/**
 * @author Omesh Deoli
 *
 * ${tags}
 */
public class RetryableFindUniqueFilenameAction extends SingleRetryAction {
	
	private String uniqueFileName;
	public String getUniqueFileName() {
		return uniqueFileName;
	}

	public void setUniqueFileName(String uniqueFileName) {
		this.uniqueFileName = uniqueFileName;
	}

	public String getEnteredFileName() {
		return enteredFileName;
	}

	public void setEnteredFileName(String enteredFileName) {
		this.enteredFileName = enteredFileName;
	}

	public String getRemoteDir() {
		return remoteDir;
	}

	public void setRemoteDir(String remoteDir) {
		this.remoteDir = remoteDir;
	}

	private String enteredFileName;
	private String remoteDir;

	
	public RetryableFindUniqueFilenameAction(SFTPConnection connection, String enteredFileName, String remoteDir,TrackedData input) {
		super(connection, remoteDir, input);
	    this.remoteDir=remoteDir;
	    this.enteredFileName=enteredFileName;
		
	}

	@Override
	public void doExecute() {
		uniqueFileName = this.getConnection().findUniqueFileName(enteredFileName, remoteDir);
	}

}
