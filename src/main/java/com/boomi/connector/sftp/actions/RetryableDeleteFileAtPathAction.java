//Copyright (c) 2020 Boomi, Inc.

package com.boomi.connector.sftp.actions;

import com.boomi.connector.api.TrackedData;

import com.boomi.connector.sftp.SFTPConnection;

/**
 * @author Omesh Deoli
 *
 *         ${tags}
 */
public class RetryableDeleteFileAtPathAction extends SingleRetryAction {

	private String fileFullPath;

	public RetryableDeleteFileAtPathAction(SFTPConnection connection, String fileFullPath, TrackedData input) {
		super(connection, fileFullPath, input);
		this.fileFullPath = fileFullPath;
	}

	@Override
	public void doExecute() {
		this.getConnection().deleteFile(fileFullPath);
	}

}
