package com.boomi.connector.sftp;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.pool.BaseKeyedPoolableObjectFactory;

import com.boomi.connector.api.ConnectorException;
import com.boomi.connector.sftp.constants.SFTPConstants;
import com.boomi.util.IOUtil;
import com.boomi.util.StringUtil;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.jce.SignatureDSA;
import com.jcraft.jsch.jce.SignatureRSA;


public class SessionFactory extends BaseKeyedPoolableObjectFactory<ConnectionProperties, Session> {
 
        /**
         * This creates a Session if not already present in the pool.
         */
        @Override
        public Session makeObject(ConnectionProperties conProp) throws Exception {
        	ByteArrayInputStream knownHosts = null;
        	Session session = null;
    		try {
    			JSch jsch = new JSch();

    			Properties config = null;

    			boolean usepk = false;
    			boolean useKnownHosts = false;
    			boolean useDHKeySize1024 = true;
    			String pkpath = null;
    			String pkpassword = null;
    			if (conProp.getSSHOptions() != null) {
    				usepk = conProp.getSSHOptions().isSshkeyauth();
    				pkpath = conProp.getSSHOptions().getSshkeypath();
    				pkpassword = conProp.getSSHOptions().getSshkeypassword();
    				useKnownHosts = !StringUtil.isBlank((String) conProp.getSSHOptions().getKnownHostEntry());
    				if (useKnownHosts) {
    					knownHosts = new ByteArrayInputStream(conProp.getSSHOptions().getKnownHostEntry().getBytes(SFTPConstants.UTF_8));
    					jsch.setKnownHosts((InputStream) knownHosts);
    				}
    				useDHKeySize1024 = conProp.getSSHOptions().isDhKeySizeMax1024();
    			}
    			if (usepk && !StringUtil.isBlank(pkpath)) {
    				try {
    					jsch.addIdentity(pkpath, pkpassword);
    				} catch (JSchException e) {
    					throw new ConnectorException(SFTPConstants.UNABLE_TO_PARSE_SSH_KEY + e.getMessage(), (Throwable) e);
    				}
    			}
    			if (conProp.getHost() != null) {

    				config = new Properties();
    				config.setProperty(SFTPConstants.PROP_STRICT_HOST_KEY_CHECKING, SFTPConstants.SHKC_NO);
    				config.setProperty(SFTPConstants.PREFERRED_AUTHENTICATIONS, SFTPConstants.AUTH_SEQUENCE_FULL);
    				config.put(SFTPConstants.KEY_COMP_S2C_ALG, SFTPConstants.COMP_ALGS);
    				config.put(SFTPConstants.KEY_COMP_C2S_ALG, SFTPConstants.COMP_ALGS);
    				config.put(SFTPConstants.SIGNATURE_DSS_KEY, SignatureDSA.class.getCanonicalName());
    				config.put(SFTPConstants.SIGNATURE_RSA_KEY, SignatureRSA.class.getCanonicalName());
    				if (useDHKeySize1024) {
    					config.put(SFTPConstants.DH_GROUP_EXCHANGE_SHA1, SFTPConstants.CLASS_DHGEX1024);
    					config.put(SFTPConstants.DH_GROUP_EXCHANGE_SHA256, SFTPConstants.CLASS_DHGEX256_1024);
    					config.put(SFTPConstants.KEX, SFTPConstants.LEGACY_ALGO_LIST);
    				}
    				if (useKnownHosts) {
    					config.put(SFTPConstants.PROP_STRICT_HOST_KEY_CHECKING, SFTPConstants.YES);
    				}
    				switch (conProp.getAuth()) {
    				case PUBLIC_KEY:
    					if (!conProp.getPubKeyParam().isUseKeyContentEnabled()) {
    						jsch.addIdentity(conProp.getPubKeyParam().getPrvkeyPath(), conProp.getPubKeyParam().getPassphrase());
    					} else {
    						jsch.addIdentity(conProp.getPubKeyParam().getKeyPairName(), conProp.getPubKeyParam().getPrvkeyContent(),
    								conProp.getPubKeyParam().getPubkeyContent(), conProp.getPubKeyParam().getPassphraseContent());

    					}
    					session = jsch.getSession(conProp.getPubKeyParam().getUser(), conProp.getHost(), conProp.getPort());
    					break;
    				case PASSWORD:
    					session = jsch.getSession(conProp.getPasswordParam().getUser(), conProp.getHost(), conProp.getPort());
    					session.setPassword(conProp.getPasswordParam().getPassword());
    					break;
    				}

    				session.setConfig(config);
    				session.setDaemonThread(true);
    				session.connect(60000);
    			}

    		} catch (UnsupportedEncodingException e) {
    			killSession(session);
    			throw new ConnectorException(SFTPConstants.UNABLE_TO_PPROCESS_KNOWN_HOST_KEY + e.getMessage(),
    					(Throwable) e);
    		} catch (JSchException e) {
    			if (session == null) {
    				throw new ConnectorException(
    						SFTPConstants.ERROR_FAILED_CREATING_SESSION + SFTPConstants.CAUSE + e.getMessage(), e);
    			}
    			if (!session.isConnected()) {
    				killSession(session);
    				if (e.getCause() instanceof java.net.UnknownHostException) {
    					throw new ConnectorException(
    							SFTPConstants.ERROR_FAILED_CONNECTION_TO_HOST + SFTPConstants.CAUSE + e.getMessage(), e);
    				}
    				if (e.getCause() instanceof java.net.ConnectException) {
    					throw new ConnectorException(
    							SFTPConstants.ERROR_FAILED_CONNECTION_TO_PORT + SFTPConstants.CAUSE + e.getMessage());
    				} else {
    					throw new ConnectorException(
    							SFTPConstants.ERROR_FAILED_SFTP_LOGIN + SFTPConstants.CAUSE + e.getMessage());
    				}
    			} else {
    				killSession(session);
    				throw new ConnectorException(SFTPConstants.ERROR_FAILED_SFTP_SERVER_CONNECTION
    						+ SFTPConstants.ERROR_FAILED_SFTP_LOGIN + SFTPConstants.CAUSE + e.getMessage(), e);
    			}
    		} catch (Exception e) {
    			killSession(session);
    			throw new ConnectorException(SFTPConstants.ERROR_FAILED_SFTP_SERVER_CONNECTION
    					+ SFTPConstants.ERROR_FAILED_SFTP_LOGIN + SFTPConstants.CAUSE + e.getMessage(), e);
    		} finally {
    			if (knownHosts != null) {
    				IOUtil.closeQuietly(knownHosts);
    			}
    			if (session == null || !session.isConnected()) {
    				killSession(session);
    			}
    		}
    		return session;
        }
        
    	private void killSession(Session session) {
    		if (session != null && session.isConnected()) {
    			try {
    				session.disconnect();
    			} catch (Exception e) {
    				Logger.getLogger(SFTPClient.class.getName()).log(Level.FINE, SFTPConstants.ERROR_DISCONNECTING_SESSION, e);
    			}
    		}
    	}
 
        /**
         * This is called when closing the pool objects
         */
        @Override
        public void destroyObject(ConnectionProperties serverDetails, Session session) {
        	killSession(session);
        }
}