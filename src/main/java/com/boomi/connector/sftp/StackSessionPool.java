package com.boomi.connector.sftp;

import org.apache.commons.pool.KeyedObjectPool;
import org.apache.commons.pool.PoolUtils;
import org.apache.commons.pool.impl.StackKeyedObjectPool;

import com.jcraft.jsch.Session;

public class StackSessionPool{
		
		
        private KeyedObjectPool<ConnectionProperties, Session> pool = null;
 
        private static class SingletonHolder {
                public static final StackSessionPool INSTANCE = new StackSessionPool();
        }
 
        public static StackSessionPool getInstance() {
                return SingletonHolder.INSTANCE;
        }
 
        private StackSessionPool()
        {
                startPool();
        }
 
        /**
         * 
         * @return the org.apache.commons.pool.KeyedObjectPool class
         */
        public KeyedObjectPool<ConnectionProperties, Session> getPool() {
                return pool;
        }
 
        /**
         * 
         * @return the org.apache.commons.pool.KeyedObjectPool class
         */
        private void startPool() {
        	pool = PoolUtils.synchronizedPool(new StackKeyedObjectPool<ConnectionProperties, Session>(new SessionFactory(), 10));
        }
        
}